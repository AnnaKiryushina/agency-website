$(document).ready(function () {
    new WOW().init();

//Строка поиска в header

    $('.search-line').click(function () {
        $('.agency-nav').hide();
        $('.search-line').css({
                'width': '660px',
                '-webkit-transition': 'all .5s',
                '-moz-transition': 'all .5s',
                'transition': 'all .5s',
                'cursor': 'text'
        });
    });

    $('.form-search').focusout(function () {
        $('.agency-nav').fadeIn();
        $('.search-line').css({
            'width': '120px',
            'background': '$darkPoly url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/PjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiIHZpZXdCb3g9IjAgMCA0NzUuMDg0IDQ3NS4wODQiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQ3NS4wODQgNDc1LjA4NDsiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxnPjxwYXRoIGQ9Ik00NjQuNTI0LDQxMi44NDZsLTk3LjkyOS05Ny45MjVjMjMuNi0zNC4wNjgsMzUuNDA2LTcyLjA0NywzNS40MDYtMTEzLjkxN2MwLTI3LjIxOC01LjI4NC01My4yNDktMTUuODUyLTc4LjA4NyAgIGMtMTAuNTYxLTI0Ljg0Mi0yNC44MzgtNDYuMjU0LTQyLjgyNS02NC4yNDFjLTE3Ljk4Ny0xNy45ODctMzkuMzk2LTMyLjI2NC02NC4yMzMtNDIuODI2ICAgQzI1NC4yNDYsNS4yODUsMjI4LjIxNywwLjAwMywyMDAuOTk5LDAuMDAzYy0yNy4yMTYsMC01My4yNDcsNS4yODItNzguMDg1LDE1Ljg0N0M5OC4wNzIsMjYuNDEyLDc2LjY2LDQwLjY4OSw1OC42NzMsNTguNjc2ICAgYy0xNy45ODksMTcuOTg3LTMyLjI2NCwzOS40MDMtNDIuODI3LDY0LjI0MUM1LjI4MiwxNDcuNzU4LDAsMTczLjc4NiwwLDIwMS4wMDRjMCwyNy4yMTYsNS4yODIsNTMuMjM4LDE1Ljg0Niw3OC4wODMgICBjMTAuNTYyLDI0LjgzOCwyNC44MzgsNDYuMjQ3LDQyLjgyNyw2NC4yMzRjMTcuOTg3LDE3Ljk5MywzOS40MDMsMzIuMjY0LDY0LjI0MSw0Mi44MzJjMjQuODQxLDEwLjU2Myw1MC44NjksMTUuODQ0LDc4LjA4NSwxNS44NDQgICBjNDEuODc5LDAsNzkuODUyLTExLjgwNywxMTMuOTIyLTM1LjQwNWw5Ny45MjksOTcuNjQxYzYuODUyLDcuMjMxLDE1LjQwNiwxMC44NDksMjUuNjkzLDEwLjg0OSAgIGM5Ljg5NywwLDE4LjQ2Ny0zLjYxNywyNS42OTQtMTAuODQ5YzcuMjMtNy4yMywxMC44NDgtMTUuNzk2LDEwLjg0OC0yNS42OTNDNDc1LjA4OCw0MjguNDU4LDQ3MS41NjcsNDE5Ljg4OSw0NjQuNTI0LDQxMi44NDZ6ICAgIE0yOTEuMzYzLDI5MS4zNThjLTI1LjAyOSwyNS4wMzMtNTUuMTQ4LDM3LjU0OS05MC4zNjQsMzcuNTQ5Yy0zNS4yMSwwLTY1LjMyOS0xMi41MTktOTAuMzYtMzcuNTQ5ICAgYy0yNS4wMzEtMjUuMDI5LTM3LjU0Ni01NS4xNDQtMzcuNTQ2LTkwLjM2YzAtMzUuMjEsMTIuNTE4LTY1LjMzNCwzNy41NDYtOTAuMzZjMjUuMDI2LTI1LjAzMiw1NS4xNS0zNy41NDYsOTAuMzYtMzcuNTQ2ICAgYzM1LjIxMiwwLDY1LjMzMSwxMi41MTksOTAuMzY0LDM3LjU0NmMyNS4wMzMsMjUuMDI2LDM3LjU0OCw1NS4xNSwzNy41NDgsOTAuMzZDMzI4LjkxMSwyMzYuMjE0LDMxNi4zOTIsMjY2LjMyOSwyOTEuMzYzLDI5MS4zNTh6ICAgIiBmaWxsPSIjRkZGRkZGIi8+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjwvc3ZnPg==")',
            'background-repeat': 'no-repeat',
            'background-position': 'center left',
            'background-size': '32px 21px',
            'height': '30px'
        });
    });
//----
// расположение видео внутри экрана ноутбука

    $('.mac-video').css({
        'top': -($('.mac-screen__img').height() * 0.049),
        'left': -($('.mac-screen__img').width() * 0.0146),
        'width': $('.mac-screen__img').width() * 0.66,
        'height': $('.mac-screen__img').height() * 0.77
    });

    $('.mac-video').attr('height', $('.mac-screen__img').height() * 0.6);
    $('.mac-video').attr('width', $('.mac-screen__img').width() * 0.3);

    //---
// red-triangle1
    var whatWeDoHeight = $('.what-we-do').height();
    var containerX0 = $('.container').offset().left;
    var redTriangle1Top = (whatWeDoHeight - 2 * (containerX0 - 116)) / 2;
    $('.red-triangle1').css({'top': redTriangle1Top, 'width': containerX0 - 186, 'height': (containerX0 - 116) * 2});
//---
    var howWeWorkHeight = $('.how-we-works').height();
    var p1_x = containerX0 * 0.29;
    var redRec2W = ($(window).width() - $('.container').width()) / 2 + p1_x;
    var hDiff = $('.container').offset().top + (redRec2W - $('.container').height()) / 2;

    //triangle4 красный треугольник из footer (ширина, высота, положение)
    var redTrian4H = $('footer').height() * 0.184;
    var redTrian4W = $(window).width() * 0.24;
    $('.red-triangle4').css({
        'width': redTrian4W,
        'height': redTrian4W / 2,
        'right': -(redTrian4H * 2) * 0.2
    });

    $('.red-triangle2').css({
        'width': redRec2W,
        'height': redRec2W * 2,
        'top': -hDiff
    });

    $(".steps-slider").slick({
        centerMode: true,
        autoplay: true,
        arrows: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplaySpeed: 3000
    });

    //team-slider стиль

    $(".team-slider").slick({
        centerMode: true,
        autoplay: true,
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplaySpeed: 1500
    });

    var memW = $('.team-slider').width() * 0.23;
    var memH = $('.team-slider').height();
    $('.team-slider-window').height(memH) + 130;
    $('.team-member').width(memW);
    $('.member-photo').css({
        'width': memW,
        'height': memW
    });
    //Убирает надписи на кнопках слайдера (our team)
    $('.slick-prev').html('');
    $('.slick-next').html('');

    //top для кнопок слайдера
    $('.team-slider').find('.slick-arrow').css({
      'top': $('.team-slider').height() * 0.4
    });

    //положение черного тругольника в our creative team
    $('.dark-triangle1').css({
        'height': $('.dark-triangle1').width() * 2,
        'top': -$('.dark-triangle1').width()
    });

    //слайдер what client says (под комментарием)
    var comment, name, photo;
    //$(".client-slider").currentIndex = 0;

    $(".client-slider").slick({
        centerMode: true,
        autoplay: true,
        arrows: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplaySpeed: 2000
    });

    var allClients = $('.client-slider').find('.client:not(.slick-cloned)');
    var fullCom, fullName, bigPhoto;

    //выводит комментарий, имя, фото в большом формате
    $(".client-slider").on('afterChange', function (event, slick, currentSlide, nextSlide) {
        $(".client-slider").currentIndex = currentSlide;

        fullCom = $(allClients[currentSlide]).find('.content').text();
        fullName = $(allClients[currentSlide]).find('.name').text();
        bigPhoto = $(allClients[currentSlide]).find('.little-client-photo').attr('src');

        $('.place-comment').html(fullCom);
        $('.place-client-name').html(fullName);
        $('.photo-frame').find('img').attr('src', bigPhoto);
    });

    $('.dark-triangle2').css({
        'height': $('.dark-triangle2').width()
    });

    $('.red-triangle3').css({
        'height': $('.red-triangle3').width() * 1.8
    });
});